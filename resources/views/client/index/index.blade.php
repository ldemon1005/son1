@extends('client.master')

@section('content')
    <section class="main-content">
        <div class="introduce"
             style="background: url({{$config && count($config->banner_index) ? $config->banner_index[0] : 'uploads/slide_1.png'}});">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 col-12 col-left">
                        <div class="col-left__content">
                            {!! $config->content_home !!}
                            <button class="btn btn-default btn-seen">
                                Xem thêm
                            </button>
                        </div>
                    </div>
                    <div class="col-md-5 col-12 col-right">
                        <img src="{{asset('client/imgs/Machines.png')}}" alt="introduce product">
                    </div>
                </div>
            </div>
        </div>

        <div class="services">
            <div class="container">
                <div class="row">
                    @foreach($services as $key => $service)
                        <div class="col-md-4 col-12 service-item">
                            <a href="{{route('detail_service_view',['slug' => $service->slug . '---' . $service->id])}}">
                                <img src="{{$service->image}}" alt="{{$service->title}}">
                                <div class="services-title bg-f3 d-flex">
                                    <div class="title w-100">
                                        <h4>{{$service->title}}</h4>
                                    </div>
                                    <div class="float-right text-center services-icon">
                                        {!! $service->icon !!}
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="hot-products">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="hot-products__title d-flex">
                            <h3><i>Sản phẩm</i></h3>
                            <div class="hot-products__title-hr">
                                <img src="{{asset('client/imgs/hr_product.png')}}" alt="Sản phẩm">
                            </div>
                        </div>
                    </div>
                    @foreach($product_home->chunk(3)[0] as $product_home)
                        <div class="col-md-4 col-12 hot-product-item">
                            <div class="hot-product-item__title">
                                <a href="#"><h3>{{$product_home->title}}</h3></a>
                            </div>
                            <div class="hot-product-item__content">
                                <div class="text">
                                    {!! $product_home->description1 !!}
                                </div>
                                <hr>
                                <div class="images">
                                    <img src="{{$product_home->image}}" alt="brand">
                                </div>
                                <div class="info-brand">
                                    {!! $product_home->description2 !!}
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>

@endsection

