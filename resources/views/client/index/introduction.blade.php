@extends('client.master')

@section('content')
    <section class="introduce-banner position-relative" style="background-image: url('{{$config->banner_introduce}}')">
        <div class="container">
            <div class="row">
                <div class="banner-text position-absolute">
                    <h3 class="company-name" style="color: #000; opacity: 0.4">{{$config->title}}</h3>
                    <h3 class="company-type">JSC</h3>
                </div>
            </div>
        </div>
    </section>

    <section class="wrap-content bg-e2">
        <div class="box-introduce">
            <div class="container">
                <div class="row pd-bt-24 hidden-xs">
                    <div class="col-md-8 introduce-content">
                        <div class="bg-f" style="padding: 60px 28px; text-align: justify;">
                            <div class="title">
                                <h3 style="font-size: 29px">Công ty Cổ phần Chế tạo máy Thăng Long,
                                    tên giao dịch là Thang Long Machine.,JSC</h3>
                            </div>
                            <p style="margin-top: 25px; font-size: 16px; opacity: 0.7">
                                Công ty được thành lập theo giấy phép kinh doanh số: 0105027520 do Sở kế hoạch và đầu tư Thành phố Hà Nội cấp ngày 03 tháng 12 năm 2010.
                                Trải qua 4 năm hoạt động và phát triển, công ty chúng tôi đang hướng tới là vị trí hàng đầu trong lĩnh vực tư vấn, cung cấp,
                                lắp đặt, bảo hành bảo trì máy phát điện và hệ thống điện.
                            </p>
                            <p style="margin-top: 25px; font-size: 18px;">
                                Các dòng sản phẩm máy phát điện truyền thống mang nhãn hiệu Thăng Long được sản xuất và lắp ráp theo các tiêu chuẩn sau:
                            </p>
                            <div class="list-rule">
                                <ul class="list-group">
                                    <li class="rule-item">
                                        <span class="number"><span>01</span></span>
                                        <span class="text">
                                        Hệ thống quản lý chất lượng ISO 9001: 2008.
                                    </span>
                                    </li>
                                    <li class="rule-item">
                                        <span class="number"><span>02</span></span>
                                        <span class="text">
                                        Đáp ứng các tiêu chuẩn:
                                        <span class="c-c22">GB/T2820,</span>
                                        <span class="c-c22">GB1103, </span>
                                        <span class="c-c22">IEC34, </span>
                                        <span class="c-c22">BS5000, </span>
                                        <span class="c-c22">ISO8525-3, </span>
                                        <span class="c-c22">ISO3046, </span>
                                        <span class="c-c22">ISO8525</span>.
                                    </span>
                                    </li>
                                    <li class="rule-item">
                                        <span class="number"><span>03</span></span>
                                        <span class="text">
                                        Tiêu chuẩn khí thải Châu âu, tiêu chuẩn EPA của Mỹ, tiêu chuẩn CARB.
                                    </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="bg-f" style="margin-top: 32px; padding: 32px 28px;">
                            <h3 style="font-size: 18px;font-weight: bold;text-transform: uppercase; margin-bottom: 24px;">Các hãng sản phẩm ĐỘNG CƠ VÀ ĐẦU PHÁT:</h3>
                            <div class="list-brand">
                                <div class="brand-item">
                                    <img src="{{ asset('client/imgs/brand_1.png')}}" alt="Hãng sản xuất" width="112px" height="20px">
                                </div>
                                <div class="brand-item">
                                    <img src="{{ asset('client/imgs/brand_2.png')}}" alt="Hãng sản xuất" width="131px" height="26px">
                                </div>
                                <div class="brand-item">
                                    <img src="{{ asset('client/imgs/brand_3.png')}}" alt="Hãng sản xuất" width="85px" height="40px">
                                </div>
                                <div class="brand-item">
                                    <img src="{{ asset('client/imgs/brand_4.png')}}" alt="Hãng sản xuất" width="40px" height="40px">
                                </div>
                                <div class="brand-item">
                                    <img src="{{ asset('client/imgs/brand_5.png')}}" alt="Hãng sản xuất" width="122px" height="26px">
                                </div>
                                <div class="brand-item">
                                    <img src="{{ asset('client/imgs/brand_6.png')}}" alt="Hãng sản xuất" width="76px" height="32px">
                                </div>
                                <div class="brand-item">
                                    <img src="{{ asset('client/imgs/brand_7.png')}}" alt="Hãng sản xuất" width="86px" height="36px">
                                </div>
                                <div class="brand-item">
                                    <img src="{{ asset('client/imgs/brand_8.png')}}" alt="Hãng sản xuất" width="46px" height="46px">
                                </div>
                                <div class="brand-item">
                                    <img src="{{ asset('client/imgs/brand_10.png')}}" alt="Hãng sản xuất" width="116px" height="32px">
                                </div>
                            </div>
                        </div>

                        <div class="bg-f" style="margin-top: 32px; padding: 32px 28px; text-align: justify;">
                            <div class="bg-black position-relative" style="height: 40px; background-color: #000">
                                <h3 class="text-uppercase" style="font-size: 16px;background-color: #000000;color: #fff; line-height: 40px; padding-left: 27px; padding-right: 27px;">Quy TRÌNH XỬ LÝ SỰ CỐ KỸ THUẬT:</h3>
                                <div class="icon-error" style="position: absolute;top: 50%;right: 10px;transform: translateY(-50%);">
{{--                                    <img src="{{asset('client/imgs/Icon_material-error.png')}}" alt="Hãng sản xuất">--}}
                                </div>
                            </div>

                            <div class="list-process" style="margin-top: 24px">
                                <div class="process-item">
                                    <div class="bg-5e">
                                        <div class="content text-center">
                                            <h3 class="time">30 phút</h3>
                                            <p>Trả lời khách hàng trong vòng 30 phút kể từ khi nhận cuộc gọi</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="process-item">
                                    <div class="bg-6a">
                                        <div class="content text-center">
                                            <h3 class="time">30 phút</h3>
                                            <p> Trả lời khách hàng trong vòng 30 phút kể từ khi nhận cuộc gọi</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="process-item">
                                    <div class="bg-b8">
                                        <div class="content text-center">
                                            <h3 class="time">30 phút</h3>
                                            <p>Trả lời khách hàng trong vòng 30 phút kể từ khi nhận cuộc gọi</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p style="margin-top: 10px; opacity: 0.8">
                                Với cơ sở vật chất, trang thiết bị hiện đại, đội ngũ kỹ sư và nhân viên chuyên nghiệp, cásản phẩm hoàn hảo từ các
                                hãng lớn của thế giới <strong>VOLVO</strong>, <strong>CUMMINS</strong>, <strong>PERKINS</strong>, <strong>DOOSAN</strong>, <strong>MITSUBISHI</strong>, <strong>SIEMENS</strong>, <strong>ABB</strong>… Trong thời gian qua Thang Long Machine.,JSC luôn là người bạn tin
                                cậy
                                của các
                                công ty và công trình trên cả nước. Chúng tôi đã khẳng định bước đi đúng đắn của mình thông qua hàng loạt các dự án của khách hàng đã được hoàn thiện và tín nhiệm.
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 box-introduce__sidebar">
                        @foreach($config->banner_post as $key => $image)

                            @if($key % 3 == 0 )
                                <div class="img-item pd-t-63 pd-bt-63 bg-c2" style="width: 80%; float: right">
                                    <img src="{{$image}}" alt="introduce banner">
                                </div>
                            @endif
                            @if($key % 3 == 1 )
                                <div class="img-item pd-t-201 pd-bt-201 bg-f" style="width: 80%; float: right">
                                    <img src="{{$image}}" alt="introduce banner">
                                </div>
                            @endif
                            @if($key % 3 == 2 )
                                <div class="img-item pd-t-201 pd-bt-201 bg-ad" style="width: 80%; float: right">
                                    <img src="{{$image}}" alt="introduce banner">
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
                <div class="row content-mb hidden-md">
                    <div class="col-12 bg-f">
                        <div class="bg-f" style="text-align: justify;">
                            <div class="title">
                                <h3 style="font-size: 24px">Công ty Cổ phần Chế tạo máy Thăng Long,
                                    tên giao dịch là Thang Long Machine.,JSC</h3>
                            </div>
                            <p style="margin-top: 25px; font-size: 14px; opacity: 0.7">
                                Công ty được thành lập theo giấy phép kinh doanh số: 0105027520 do Sở kế hoạch và đầu tư Thành phố Hà Nội cấp ngày 03 tháng 12 năm 2010.
                                Trải qua 4 năm hoạt động và phát triển, công ty chúng tôi đang hướng tới là vị trí hàng đầu trong lĩnh vực tư vấn, cung cấp,
                                lắp đặt, bảo hành bảo trì máy phát điện và hệ thống điện.
                            </p>
                            <p style="margin-top: 25px; font-size: 16px;">
                                Các dòng sản phẩm máy phát điện truyền thống mang nhãn hiệu Thăng Long được sản xuất và lắp ráp theo các tiêu chuẩn sau:
                            </p>
                            <div class="list-rule">
                                <ul class="list-group">
                                    <li class="rule-item">
                                        <span class="number"><span>01</span></span>
                                        <span class="text">
                                        Hệ thống quản lý chất lượng ISO 9001: 2008.
                                    </span>
                                    </li>
                                    <li class="rule-item">
                                        <span class="number"><span>02</span></span>
                                        <span class="text">
                                        Đáp ứng các tiêu chuẩn:
                                        <span class="c-c22">GB/T2820,</span>
                                        <span class="c-c22">GB1103, </span>
                                        <span class="c-c22">IEC34, </span>
                                        <span class="c-c22">BS5000, </span>
                                        <span class="c-c22">ISO8525-3, </span>
                                        <span class="c-c22">ISO3046, </span>
                                        <span class="c-c22">ISO8525</span>.
                                    </span>
                                    </li>
                                    <li class="rule-item">
                                        <span class="number"><span>03</span></span>
                                        <span class="text">
                                        Tiêu chuẩn khí thải Châu âu, tiêu chuẩn EPA của Mỹ, tiêu chuẩn CARB.
                                    </span>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="bg-f" style="margin-bottom: 40px; margin-top: 40px ">
                            <h3 style="font-size: 18px;font-weight: bold;text-transform: uppercase; margin-bottom: 24px;">Các hãng sản phẩm ĐỘNG CƠ VÀ ĐẦU PHÁT:</h3>
                            <div class="list-brand">
                                <div class="brand-item">
                                    <img src="{{ asset('client/imgs/brand_1.png')}}" alt="Hãng sản xuất" width="78px" height="14px">
                                </div>
                                <div class="brand-item middle-item">
                                    <img src="{{ asset('client/imgs/brand_2.png')}}" alt="Hãng sản xuất" width="80px" height="16px">
                                </div>
                                <div class="brand-item">
                                    <img src="{{ asset('client/imgs/brand_3.png')}}" alt="Hãng sản xuất" width="60px" height="28px">
                                </div>
                                <div class="brand-item middle-item">
                                    <img src="{{ asset('client/imgs/brand_5.png')}}" alt="Hãng sản xuất" width="85px" height="18px">
                                </div>
                                <div class="brand-item">
                                    <img src="{{ asset('client/imgs/brand_6.png')}}" alt="Hãng sản xuất" width="67px" height="28px">
                                </div>
                                <div class="brand-item">
                                    <img src="{{ asset('client/imgs/brand_7.png')}}" alt="Hãng sản xuất" width="67px" height="28px">
                                </div>
                                <div class="brand-item">
                                    <img src="{{ asset('client/imgs/brand_4.png')}}" alt="Hãng sản xuất" width="26px" height="26px">
                                </div>
                                <div class="brand-item middle-item">
                                    <img src="{{ asset('client/imgs/brand_8.png')}}" alt="Hãng sản xuất" width="30px" height="30px">
                                </div>
                                <div class="brand-item">
                                    <img src="{{ asset('client/imgs/brand_10.png')}}" alt="Hãng sản xuất" width="73px" height="20px">
                                </div>
                            </div>
                        </div>

                        <div class="bg-f">
                            <div class="bg-black position-relative" style="height: 40px; background-color: #000">
                                <h3 class="text-uppercase" style="font-size: 15px;background-color: #000000;color: #fff; line-height: 40px; padding-left: 15px; padding-right: 27px;">
                                    Quy TRÌNH XỬ LÝ SỰ CỐ KỸ THUẬT:
                                </h3>
                            </div>

                            <div class="list-process" style="margin-top: 24px">
                                <div class="process-item">
                                    <div class="bg-5e">
                                        <div class="content text-center">
                                            <h3 class="time">30 phút</h3>
                                            <p>Trả lời khách hàng trong vòng 30 phút kể từ khi nhận cuộc gọi</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="process-item">
                                    <div class="bg-6a">
                                        <div class="content text-center">
                                            <h3 class="time">30 phút</h3>
                                            <p> Trả lời khách hàng trong vòng 30 phút kể từ khi nhận cuộc gọi</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="process-item">
                                    <div class="bg-b8">
                                        <div class="content text-center">
                                            <h3 class="time">30 phút</h3>
                                            <p>Trả lời khách hàng trong vòng 30 phút kể từ khi nhận cuộc gọi</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p style="margin-top: 10px; opacity: 0.8; font-size: 14px">
                                Với cơ sở vật chất, trang thiết bị hiện đại, đội ngũ kỹ sư và nhân viên chuyên nghiệp, cásản phẩm hoàn hảo từ các
                                hãng lớn của thế giới <strong>VOLVO</strong>, <strong>CUMMINS</strong>, <strong>PERKINS</strong>, <strong>DOOSAN</strong>, <strong>MITSUBISHI</strong>, <strong>SIEMENS</strong>, <strong>ABB</strong>… Trong thời gian qua Thang Long Machine.,JSC luôn là người bạn tin
                                cậy
                                của các
                                công ty và công trình trên cả nước. Chúng tôi đã khẳng định bước đi đúng đắn của mình thông qua hàng loạt các dự án của khách hàng đã được hoàn thiện và tín nhiệm.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

