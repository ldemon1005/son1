@extends('admin.master')

@section('content')
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <form  action="{{$product_home ? route('admin_update_product_home_action') : route('admin_create_product_home_action')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            @if($product_home)
                <input hidden name="id" value="{{$product_home->id}}">
            @endif
            <div class="row form-group">
                <div id="Product_Content" class="col-12 col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THÔNG TIN DANH SÁCH SẢN PHẨM TRANG CHỦ</h4>
                            <div class="form-group">
                                <label for="product_home_title">Tiêu đề <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="title" id="product_home_title" aria-describedby="helpProductTitle" placeholder="Nhập tiêu đề sản phẩm"
                                       autocomplete="off" value="{{$product_home ? $product_home->title : ''}}" required>
                                <small id="helpProductTitle" class="form-text text-muted">Tiêu đề sản phẩm là bắt buộc</small>
                            </div>
                            <div class="form-group">
                                <label for="product_home_title_mobile">Tiêu đề mobile <span class="text-danger">*</span></label>
                                <input type="text" class="form-control" name="title_mobile" id="product_home_title_mobile" aria-describedby="helpProductTitle" placeholder="Nhập tiêu đề sản phẩm trên mobile"
                                       autocomplete="off" value="{{$product_home ? $product_home->title_mobile : ''}}" required>
                                <small id="helpProductTitle" class="form-text text-muted">Tiêu đề sản phẩm trên mobile là bắt buộc</small>
                            </div>
                            <div class="form-group">
                                <label for="product_home_description">Mô tả ngắn 1</label>
                                <textarea class="form-control mce_editor" name="description1" id="product_home_description1" rows="1">{{$product_home ? $product_home->description1 : ''}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="product_home_description">Mô tả ngắn 2</label>
                                <textarea class="form-control mce_editor" name="description2" id="product_home_description2" rows="1">{{$product_home ? $product_home->description2 : ''}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="category_description">Vị trí</label>
                                <input type="text" class="form-control" name="order_index" id="product_order_index" aria-describedby="helpProductTitle" placeholder="Nhập vị trí sắp xếp sản phẩm trang chủ"
                                       autocomplete="off" value="{{$product_home ? $product_home->order_index : ''}}">
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">CẤU HÌNH SEO</h4>
                            <div class="row form-group">
                                <label class="col-md-2 text-left" for="product_home_seo_title">SEO Tiêu đề</label>
                                <div class="col-md-10">
                                    <input type="text" name="seo_title" id="product_home_seo_title" class="form-control" maxlength="70"
                                           placeholder="" value="{{$product_home ? $product_home->seo_title : ''}}" autocomplete="off">
                                </div>
                            </div>
                            <div class="row form-group">
                                <label class="col-md-2 text-left" for="product_home_seo_title">SEO Mô tả</label>
                                <div class="col-md-10">
                                   <textarea name="seo_description" id="product_home_seo_description" class="form-control" rows="3"
                                             maxlength="170" placeholder="" autocomplete="off">{{$product_home ? $product_home->seo_description : ''}}</textarea>
                                </div>
                            </div><div class="row form-group">
                                <label class="col-md-2 text-left" for="product_home_seo_title">SEO Từ khóa</label>
                                <div class="col-md-10">
                                    <input type="text" name="seo_keyword" id="product_home_seo_keyword" class="form-control" data-role="tagsinput"
                                           placeholder="" value="{{$product_home ? $product_home->seo_keyword : ''}}" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="Product_Sidebar" class="col-12 col-md-3">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">ẢNH ĐẠI DIỆN</h4>
                            @include('admin.layouts.image_preview', ['input_name' => 'image', 'input_id' => 'product_home_image', 'input_image' => $product_home ? $product_home->image : ''])
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <div class="row form-group" style="padding: 10px 0">
                                <div class="col-md-4">
                                    <label>Trạng thái</label>
                                </div>
                                <div class="col-md-8">
                                    <div class="custom-control custom-switch">
                                        <input type="checkbox" class="custom-control-input" name="status" id="customSwitch1" {{$product_home && $product_home->status == 1 ? 'checked' : ''}}>
                                        <label class="custom-control-label" for="customSwitch1">Hoạt động</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h4 class="card-title font-bold">THAO TÁC</h4>

                            <button type="submit" class="btn btn-info"> Lưu</button>
                            <a type="button" class="btn btn-danger" href="{{route('admin_list_product_home')}}"> Hủy</a>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <!-- ============================================================== -->
        <!-- End PAge Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Container fluid  -->
    <!-- ============================================================== -->

@endsection

@section('admin_css')
    <link href="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css')}}" rel="stylesheet">
@endsection

@section('admin_script')
    <script src="{{asset('admin/assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js')}}"></script>
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset('admin/assets/plugins/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('admin/main/js/cuongdev-form.js')}}"></script>

    <script>
        function addBannerHome(index){
            var name_class = '.banner-' + index + ':first';
            var element = $( name_class);
            var id = parseInt(element.find('input').attr('id'));
            console.log(id);
            var new_id = id + 1;
            var element_new = element.clone();
            var regex_str = new RegExp(id.toString(),"g");
            element_new.html(element_new.html().replace(regex_str,new_id));
            console.log(element_new.html())
            element_new.prependTo( "#banner-" + index );
        }
    </script>
@endsection
