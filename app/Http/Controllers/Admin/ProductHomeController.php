<?php

namespace App\Http\Controllers\Admin;

use App\Models\BaseModel;
use App\Repositories\ProductHomeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;

class ProductHomeController extends Controller
{
    protected $productHomeRepository;

    public function __construct(ProductHomeRepository $productHomeRepository)
    {
        $this->productHomeRepository = $productHomeRepository;
        View::share('type_menu', 'config');
    }

    public function listView(Request $request){
        $params = [
            'keyword' => '',
        ];
        $query = $request->only('keyword', 'status');
        $params = array_merge($params, $query);
        if(isset($params['status']) && $params['status'] == 9){
            unset($params['status']);
        }
        $list_product_home = $this->productHomeRepository->getList($params);
        return view('admin.product_home.list_view', compact('list_product_home','params'));
    }

    public function updateProductHomeView($id = null){
        $product_home = null;
        if($id != 0){
            $product_home = $this->productHomeRepository->getByID($id);

            if(!$product_home){
                $this->resFail(null, 'Không tìm thấy sản phẩm');
            }
        }
        return view('admin.product_home.form', compact('product_home'));
    }

    public function createProductHome(Request $request){
        $validator = Validator::make($request->input(), [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors('Vui lòng điền trường bắt buộc!');
        }

        $product_home_data = $request->only('id','title','status', 'content','description1','description2', 'seo_title','seo_description','seo_keyword','image','slide_image','title_mobile','order_index');

        if(isset($product_home_data['status']) && $product_home_data['status'] == 'on'){
            $product_home_data['status'] = 1;
        }else{
            $product_home_data['status'] = 0;
        }

        try{
            $product_home = $this->productHomeRepository->createProductHome($product_home_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Tạo mới không thành công!');
        }

        if(!$product_home){
            return redirect()->back()->withErrors('Tạo mới không thành công!');
        }

        return redirect()->route('admin_list_product_home')->with(BaseModel::ALERT_SUCCESS, 'Tạo mới thành công!');
    }

    public function updateProductHome(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required',
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors('Vui lòng điền trường bắt buộc!');
        }

        $product_home_data = $request->only('id','title','status', 'content','description1','description2', 'seo_title','seo_description','seo_keyword','image','slide_image','title_mobile','order_index');

        $product_home_id = $request->get('id');

        unset($product_home_data['id']);

        if(isset($product_home_data['status']) && $product_home_data['status'] == 'on'){
            $product_home_data['status'] = 1;
        }else{
            $product_home_data['status'] = 0;
        }

        try{
            $product_home = $this->productHomeRepository->updateProductHome($product_home_id, $product_home_data);
        }catch (\Exception $exception){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        if(!$product_home){
            return redirect()->back()->withErrors('Update không thành công!');
        }

        return redirect()->route('admin_list_product_home')->with(BaseModel::ALERT_SUCCESS, 'Update thành công!');
    }

    public function deleteProductHome(Request $request){
        $validator = Validator::make($request->input(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->resFail(null, $validator->errors());
        }
        $product_home_data = $request->only('id');

        try {
            $product_home_deleted = $this->productHomeRepository->deleteProductHome($product_home_data['id']);
        } catch (\Exception $e) {
            return $this->resFail(null, $e->getMessage());
        }

        if (!$product_home_deleted) {
            return $this->resFail(null, 'Xoá không thành công. Vui lòng thử lại!');
        }

        return $this->resSuccess($product_home_deleted, 'Xoá thành công!');

    }
}
