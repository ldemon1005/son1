<?php

namespace App\Repositories;

use App\Models\ProductHome;
use Prettus\Repository\Eloquent\BaseRepository;
use DB;
use Exception;
use Prettus\Validator\Exceptions\ValidatorException;

class ProductHomeRepository extends BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return 'App\Models\ProductHome';
    }

    public function getList($params = [], $limit = 20)
    {
        $limit = !empty($params['limit']) && intval($params['limit']) != $limit ? intval($params['limit']) : $limit;
        return ProductHome::where(function ($query) use ($params) {
                if (!empty($params['keyword'])) {
                    $query->where('title','like','%' . $params['keyword'] . '%');
                }

                if (!empty($params['status']) || isset($params['status'])) {
                    $query->where('status', '=', intval($params['status']));
                }
            })->orderBy($params['order_by'] ?? 'created_at', $params['order_direction'] ?? 'desc')->paginate($limit);
    }

    public function getByID($product_home_id)
    {
        return ProductHome::where('id', '=', $product_home_id)->first();
    }

    public function findByProductHome($product_home_name)
    {
        return ProductHome::where('name', '=', $product_home_name)->first();
    }

    /**
     * @param $params
     * @return ProductHome
     * @throws Exception
     */
    public function createProductHome($params)
    {
        try {
            DB::beginTransaction();
            $params['created_at'] = time();
            $params['updated_at'] = time();
            $product_home = $this->create($params);
            DB::commit();
            return $product_home;
        } catch (Exception $e) {
            DB::rollBack();
            throw new Exception('Error: Insert DB');
        }
    }

    /**
     * @param $id
     * @param array $params
     * @return ProductHome
     * @throws Exception
     */
    public function updateProductHome($id, $params = [])
    {
        try {
            $params['updated_at'] = time();
            $updated_product_home = $this->update($params, $id);
            return $updated_product_home;
        } catch (ValidatorException $e) {
            throw new Exception($e->getMessage());
        }

    }

    public function deleteProductHome($id)
    {
        $res = $this->delete($id);
        return $res;
    }
}
