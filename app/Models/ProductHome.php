<?php

namespace App\Models;

class ProductHome extends BaseModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'product_home';
}
